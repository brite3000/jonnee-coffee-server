from pywallet import wallet
import blockcypher
import db

import requests
import urllib.request
import json

# Etherscan.io API key
etherscanApi = "ZA9SWJN75RP516AS6DH5FYVFWZZIVRSVI8"

# Blockcypher.com API key
blockCypherToken = "45cf7024de52446cb0d8de2da7f56cf7"


# Ethereum 12 word seed, which is used to generate the extended public key. If this seed is # exposed, all Ethereum held on the seed is compromised
seedETH = "spoil minor dose also garment pause museum atom frame work token fetch"

w = wallet.create_wallet(network="ETH", seed=seedETH, children=1)
xPublicKey = w["xpublic_key"]
#print(xPublicKey)


"""
Now that we've derived an extended public key from out hierarchical deterministic wallet, we can create
As many BTC/ETH address as we like, knowing out seed is safe. As long as the
only thing stored in our python file is the "xpublic_key"
"""
def getEthAddress(machineId):
    return wallet.create_address(network="ETH", xpub=xPublicKey, child=machineId)["address"]


# Query the etherscan API to get the balance for an Ethereum address
def getEthereumAddressBalance(address):
    request = urllib.request.urlopen("https://api.etherscan.io/api?module=account&action=balance&address=" + address + "&tag=latest&apikey=" + etherscanApi).read()
    request = str(request, 'utf-8')
    request = json.loads(request)
    if request["message"] == "OK":
        return request["result"]
    else:
        return -1


# Query the BtcMarkets API to get the current price of Ethereum in $AUD
def getEthPriceInAud():
    domain = "https://api.btcmarkets.net"
    uri = "/market/ETH/AUD/tick"
    url = domain + uri
    r = requests.get(url, verify=True)
    return int(r.json()["lastPrice"])



def convertAudToEth(amountInFiat):
    return amountInFiat / getEthPriceInAud()

#print(getEthAddress(5))5