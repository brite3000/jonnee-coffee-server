import unittest
import requests
from unittest.mock import Mock,patch
from db import getOrder, getUser, setOrderAsPaid, getPaidStatus, getAllDrinks

class TestDb(unittest.TestCase):
    #These tests check that getOrder() from the db.py file are working
    #When a vaild order is searched for in the db, the function returns an object
    #When an invalid order is searched for, the function returns -1
    def test_getOrderSuccess(self):
        self.assertIsNotNone(getOrder("56f6188b-e21f-4749-a1f5-603c3a1cae0f"))
    def test_getOrderFail(self):
        self.assertEqual(getOrder("56f6188b-e21f-4749zzz-a1f5-603c3a1cae0f"), -1)
    
    #Tests to check the getUser() function in db.py
    #Checking for a user with a vaild id will return an object, while checking
    #for a user with an invalid id will return -1
    def test_getUserSuccess(self):
        self.assertIsNotNone(getUser("brite3000@gmail.com"))
    def test_getUserFail(self):
       self.assertEqual(getUser("brite3000@gmail..com"), -1) 

    #Tests to check that setOrderAsPaid functions correctly from db.py
    #Setting the paid attribute to 1 in a valid order returns a 1
    #Trying to set paid to 1 in an invalid order returns -1
    def test_setOrderAsPaidSuccess(self):
        self.assertEqual(setOrderAsPaid("080484e5-9874-44a9-aa14-609923a86fbe"), 1)
    def test_setOrderAsPaidFail(self):
        self.assertEqual(setOrderAsPaid("080484e5-9874-44a9-aa14-609923a86fbezzz"), -1)
    
    #Tests to check that getPaidStatus is working correctly
    #Checking the paid status of a valid order returns 1
    #Checking the paid status of an invalid order returns -1
    def test_getPaidStatusSuccess(self):
        self.assertEqual(getPaidStatus("080484e5-9874-44a9-aa14-609923a86fbe"), 1)
    def test_getPaidStatusFail(self):
        self.assertEqual(getPaidStatus("080484e5-9874-44a9-aa14-609923a86fbezzz"), -1)
    

    #Checking the getAllDrinks function is working correctly
    #This function will always return an array
    def test_getAllDrinks(self):
        self.assertIsNotNone(getAllDrinks())



    

