import pymongo
import uuid

url = "mongodb+srv://Z:JonneeCoffee@cluster0-xgfiw.mongodb.net/JonneeCoffee";
myclient = pymongo.MongoClient(url)

mydb = myclient["JonneeCoffeeTwo"]

"""
initDb() Will setup a fresh database and will add sample documents and collections

Args:
    None

Returns:
    None

Raises:
    None
"""
def initDb():
    dblist = myclient.list_database_names()
    if "JonneeCoffeeTwo" in dblist:
        print("The database exists.")
    else:
        print("Database does not exist, creating")
        mycol = mydb["drink"]
        drink = { "id": 1, "name": "Hot Chocolate", "type": "Chocolate" }
        x = mycol.insert_one(drink)

        mycol = mydb["order"]
        order = { "vend_id": 1, "drink_id": 1, "sugar": 1, "milk": 1, "byo_cup": 1 }
        x = mycol.insert_one(order)

        mycol = mydb["user"]
        user = { "email": "brite3000@gmail.com", "hash": "abcd123" }
        x = mycol.insert_one(user)

        mycol = mydb["vending_machine"]
        vendingMachine = { "id": 1, "status": 1, "location": "Flinders Station", "api_key": "123456" }
        x = mycol.insert_one(vendingMachine)

        print(mydb.list_collection_names())

"""
getOrder() will grab a specifc order from the database

Args:
    orderId: The order id of the document you would like to retreive.

Returns:
    A JSON object containing the order if the order is found OR,
    the int -1 if no document with that orderId is found.

Raises:
    None
"""

def getOrder(orderId):
    mycol = mydb["order"]
    myquery = { "_id": orderId }
    myDoc = mycol.find(myquery)
    if myDoc.count() > 0:
        return myDoc[0]
    else:
        return -1

"""
createOrder() Will add a new order document to the database

Args:
    order: The JSON object of the order to be added to the database

Returns:
    None

Raises:
    None
"""

def createOrder(order):
    mycol = mydb["order"]
    x = mycol.insert_one(order)

"""
createUser() Will add a user to the database

Args:
    email: The email of the user
    passwordHash: The hashed password of the user

Returns:
    None

Raises:
    None
"""

def createUser(email, passwordHash):
    mycol = mydb["user"]
    user = { "email": email, "hash": passwordHash }
    x = mycol.insert_one(user)

def addEmail(userInfo):
    mycol = mydb["email"]
    x = mycol.insert_one(userInfo)

"""
getUser() will grab a specifc user from the database

Args:
    email: the email of the user

Returns:
    A JSON object containing the user if the user is found OR,
    the int -1 if no document with that email is found.

Raises:
    None
"""

def getUser(email):
    mycol = mydb["user"]
    myquery = { "email": email }
    myDoc = mycol.find(myquery)
    if myDoc.count() > 0:
        return myDoc[0]
    else:
        return -1

def getCryptoWalletIndex():
    mycol = mydb["cryptocurrency"]
    myquery = { "_id": "wallet_index" }
    myDoc = mycol.find(myquery)
    if myDoc.count() > 0:
        return myDoc[0]
    else:
        return -1

"""
getPaidStatus() check to see if an order has been paid via debit/credit

Args:
    orderId: The order id of the document you would like to retreive.

Returns:
    The int 1 if the order has been paid, OR
    the int -1 if the order has not been paid

Raises:
    None
"""

def getPaidStatus(orderId):
    mycol = mydb["order"]
    myquery = { "id": orderId }
    myDoc = mycol.find(myquery)
    if myDoc.count() > 0:
        return myDoc[0]["paid"]
    else:
        return -1

"""
setOrderAsPaid() will set the paid attribute in a document to 1

Args:
    orderId: The order id of the document you would like to retreive.

Returns:
    The int 1 if the document was found and the paid attribute was updated, OR
    the int -1 if a document with that order id was not found.

Raises:
    None
"""

def setOrderAsPaid(orderId):
    mycol = mydb["order"]
    myquery = { "id": orderId }
    newvalues = { "$set": { "paid": 1 } }
    mydoc = mycol.find(myquery)
    if mydoc.count() > 0:
        mycol.update_one(myquery, newvalues)
        return 1
    else:
        return -1

"""
getAllDrinks() will get the whole collection of drinks

Args:
    None

Returns:
    An Array containing the JSON object of each drink

Raises:
    None
"""

def getAllDrinks():
    mycol = mydb["drink"]
    drinkArray = [""]
    for x in mycol.find():
        drinkArray.insert(0, x)
    return drinkArray



#initDb() 
#print(createOrder("1", "1", "5", "1", "1")) 
#print(createUser("brite3001@gmail.com", "qwerty"))

#print(getOrder("56f6188b-e21f-4749-a1f5-603c3a1cae0f")) WORKS RETURNS OBJECT
#print(getOrder("56f6188b-e21f-4749zzz-a1f5-603c3a1cae0f")) FAILS RETURNS -1

#print(getUser("brite3000@gmail..com")) FAILS RETURNS -1
#print(getUser("brite3000@gmail..com")) PASS RETURNS OBJECT

#print(setOrderAsPaid("080484e5-9874-44a9-aa14-609923a86fbe")) WORKS RETURNS 1
#print(setOrderAsPaid("080484e5-9874ZZZ-44a9-aa14-609923a86fbe")) FAILS RETURNS -1

#print(getPaidStatus("080484e5-9874-44a9-aa14-609923a86fbe")) WORKS RETURNS 1
#print(getPaidStatus("080484e5-9874ZZZ-44a9-aa14-609923a86fbe")) FAIL RETURN -1

#print(getAllDrinks()) RETURNS AN ARRAY,

print(getAllDrinks())