import ethereumWallet
import bitcoinWallet
import db
import time
import datetime
import json
from flask import Flask, flash, redirect, render_template, request, session, abort
from flask_cors import CORS, cross_origin


# getTimeStamp() returns the time in ISO-8601
def getTimeStamp():
    return datetime.datetime.utcnow().replace(tzinfo=datetime.timezone.utc).isoformat()
 
app = Flask(__name__)


# This enables support for Cross origin resource sharing
CORS(app, support_credentials=True)

"""
getOrderForVend() is the function which responds to the flask route "/placeOrder."
This route expects to receive an order which is sent as bytes, which is then parsed as a utf-8 string.
The order is then checked for payment_type:

    if an order is of type BTC, a unique Bitcoin address will be generated via bitcoinWallet.py,
    the $AUD cost of the order will be converted to its bitcoin equivilent in satoshis.
    The order received from the frontend will have this new information added to the
    JSON object.
    Once this is finished a reponse with the format 
    {"status": 2, "address": btcAddr, "cost": costBtc} will be sent to the frontend

    if an order is of type ETH, a unique Ethereum address will be generated via ethereumWallet.py,
    the $AUD cost of the order will be converted to its Ethereum equivilent in wei.
    The order received from the frontend will have this new information added to the
    JSON object.
    Once this is finished a reponse with the format 
    {"status": 2, "address": ethAddr, "cost": costEth} will be sent to the frontend

    if the order is of type CARD status 1 will be sent, which signals the front end that
    the order has been paid for **TO BE IMPLEMENTED WITH CARD PROCESSOR**

Args:
    None

Returns:
    A response depending on the payment type of the order as a JSON object

Raises:
    None
"""

@app.route("/placeOrder", methods = ["PUT"])
def getOrderForVend():
    print("Place order started, id:")
    req = request.data
    order = str(req, "utf-8")
    order = json.loads(order)
    print(order["_id"])
    response = {}
    if order['payment_type'] == "ETH":  
        print("Payment type is eth")
        ethAddr = ethereumWallet.getEthAddress(order["vend_id"]) ####
        order['address'] = ethAddr
        costEth = round(ethereumWallet.convertAudToEth(order["cost_aud"]), 4)
        order['cost'] = costEth
        order['server_order_submission'] = getTimeStamp()
        response = {"status": 2, "address": ethAddr, "cost": costEth}
    elif order["payment_type"] == "BTC":
        print("Payment type is btc")
        btcAddr = bitcoinWallet.genBtcBlockCypherTestnetAddr()
        order['address'] = btcAddr
        costBtc = round(bitcoinWallet.convertAudToBtc(order["cost_aud"]), 6)
        order['cost'] = costBtc
        order['server_order_submission'] = getTimeStamp()
        response = {"status": 2, "address": btcAddr, "cost": costBtc}
    elif order["payment_type"] == "CARD":
        print("Payment type is card")
        timestamp = getTimeStamp()
        order['server_order_submission'] = timestamp
        response = {"status": 1}
    else:
        print("Payment type is unknown")
        timestamp = getTimeStamp()
        order['server_order_submission'] = timestamp
        response = {"status": -1}
    db.createOrder(order)
    #print(response)
    return (json.dumps(response))


"""
checkPaidCryptocurrency() is the function which is invoked from the flask route /checkPayment
This function expects to receive an order in the form of JSON object transmitted in bytes.
The function decodes the order as a utf-8 string, then parses it as a JSON.

the function checks the payment_type of the order, then sends the order off to another function depending on its payment_type. e.g. an order of type BTC will be sent to the function checkIfBtcAddressHasBeenPaid.


Args:
    None

Returns:
    The JSON object {"status": 1} if the order has been paid for, OR
    the JSON object {"status": -1} if an error occurs

Raises:
    None
"""

@app.route("/checkPayment", methods = ["PUT"])
def checkPaidCryptocurrency():
    print("check payment started for order:")
    req = request.data
    order = str(req, "utf-8")
    order = json.loads(order)
    print(order["_id"])
    
    if order["payment_type"] == "ETH":
        return checkIfEthAddressHasBeenPaid(order)
    elif order["payment_type"] == "BTC":
        return checkIfBtcAddressHasBeenPaid(order)
    elif order["payment_type"] == "CARD":
        return json.dumps({"status": 1})
    else:
        return json.dumps({"status": -1})


@app.route("/addEmail", methods = ["PUT"])
def addEmailToDb():
    print("check payment started for order:")
    req = request.data
    userInfo = str(req, "utf-8")
    userInfo = json.loads(userInfo)
    print(userInfo)
    db.addEmail(userInfo)
    return json.dumps({"status": 1})

"""
checkIfBtcAddressHasBeenPaid() is a helper function of CheckPaidCryptocurrency()
This function expects a JSON order object, and then starts a while loop.
Inside this will loop, the function getBitcoinAddressBalance() from cryptocurrency.py continuly checks the Bitcoin address its been passed, for the balance that it contains on the blockchain. Some other metadata about this particular Bitcoin address are also parsed, including the 'double spend' checker:

    if the balance is equal to the amount quoted in the order AND the number of transactions associated with this bitcoin address is more than 1 AND a double spend attack has not been detected, set response = {"status": 1} and break the lop
    
    elif a double spend attack has been detected, set response = {"status": -1} and break the loop



Args:
    order: A JSON object that represents an order

Returns:
    The JSON object {"status": 1} if the order has been paid for, OR
    the JSON object {"status": -1} if an error occurs

Raises:
    None
"""

def checkIfBtcAddressHasBeenPaid(order):
    btcAddress = order["address"]
    print("Checking btc address: " + btcAddress)
    amountDue = order["cost"]
    response = {}
    i = 0
    while(i < 100):
        addressInfo = bitcoinWallet.getTestnetBlockCypherBalance(btcAddress)
        balance = addressInfo["balance"]
        numberOfTransactions = addressInfo["ntx"]
        doubleSpend = addressInfo["doubleSpend"]
        print(amountDue, balance)
        if balance >= amountDue and numberOfTransactions > 0 and doubleSpend == "False":
            print("Bitcoin paid to address successfully!")
            response = {"status": 1}
            break
        elif doubleSpend == "True":
            print("Double spend detected!!!")
            response = {"status": -1}
            break
        i += 1
        print("Not paid, time taken: " + str((i * 2)))
        time.sleep(2)
    return json.dumps(response)

"""
checkIfEthAddressHasBeenPaid() is a helper function of CheckPaidCryptocurrency()
This function expects an JSON order object, and then starts a while loop.
Inside this will loop, the function getEthereumAddressBalance() from cryptocurrency.py continuly checks the Ethereum address its been passed, for the balance that it contains on the blockchain.

    if the balance is equal to the amount quoted in the order, set response = {"status": 1} and break the lop
    
The default value for response = {"status": -1}, so if the Ethereum address is not funded within about 200 seconds, this reponse will be returned



Args:
    order: A JSON object that represents an order

Returns:
    The JSON object {"status": 1} if the order has been paid for, OR
    the JSON object {"status": -1} if an error occurs

Raises:
    None
"""

def checkIfEthAddressHasBeenPaid(order):
    ethAddress = order["address"]
    print("Checking ethereum address: " + ethAddress)
    amountDue = float(order["cost"])
    response = {"status": -1}
    i = 0
    previousBalance = float(ethereumWallet.getEthereumAddressBalance(ethAddress))
    while(i < 100):
        newBalance = float(ethereumWallet.getEthereumAddressBalance(ethAddress))
        print("amount due: {0}, previous balance: {1}, current balance: {2}".format(amountDue, previousBalance, newBalance))
        if (newBalance - previousBalance) >= amountDue:
            print("Ethereum paid to address successfully!")
            response = {"status": 1}
            break
        i += 1
        print("Not paid, time taken: " + str((i * 2)))
        time.sleep(2)
    return json.dumps(response)

    



if __name__ == "__main__":
    app.run(host='0.0.0.0', port=3002)




#myOrder = getOrder("080484e5-9874-44a9-aa14-609923a86fbe")
#print(getTimeStamp())
#checkIfBtcAddressHasBeenPaid(myOrder)

#myEthAddr = genEthAddr()
#print(myEthAddr)

#print(getEthereumAddressBalance("0xc118bc23f2569f68086dc2e2c3184abf3a2b6317"))

#print(getEthereumAddressBalance("0xf0b776a220dcaf9c5f72f6835bef457cbe54d124"))

#checkIfEthAddressHasBeenPaid(myOrder)




    
