# Bitcoin API provided by blockchain.info
from blockchain import blockexplorer

# Create bitcoin addresses
from pywallet import wallet

# Bitcoin / Ethereum / Bitcoin test API provided by blockcypher.com
import blockcypher

import requests
import urllib.request
import json

# Blockcypher.com API key
blockCypherToken = "45cf7024de52446cb0d8de2da7f56cf7"

# Bitcoin 12 word seed, which is used to generate the extended public key. If this seed is exposed all Bitcoin held on this seed is compromised
seedBTC = "script rude crane cycle frozentravel monkey sausage ride soon fix deliver"

w = wallet.create_wallet(network="BTC", seed=seedBTC, children=1)
xPublicKey = w["xpublic_key"]


"""
Now that we've derived an extended public key from out hierarchical deterministic wallet, we can create
As many BTC/ETH address as we like, knowing out seed is safe. As long as the
only thing stored in our python file is the "xpublic_key"
"""

# Generate a bitcoin address
def getBitcoinAddress(walletIndex):
    return wallet.create_address(network="BTC", xpub=xPublicKey, child=walletIndex)["address"]

# Communicates with the blockcypher API and returns the balance of a Bitcoin test address
def genBtcBlockCypherTestnetAddr():
    req = "http://api.blockcypher.com/v1/bcy/test/addrs?token=" + blockCypherToken
    response = requests.post(req).content
    response = str(response, "utf-8")
    response = json.loads(response)
    return response["address"]

# Communicates with the block cypher API to get the balance of a Bitcoin test address,
# also check for double spends
def getTestnetBlockCypherBalance(address):
    response = {}
    endpoint = "http://api.blockcypher.com/v1/bcy/test/addrs/" 
    url = endpoint + address + "?token=" + blockCypherToken
    r = requests.get(url)
    balance = r.json()["balance"]
    ntx = r.json()["n_tx"]
    if ntx == 0:
        response = {"balance": balance, "doubleSpend": "False", "ntx": ntx}
    elif ntx == 1:
        doubleSpend = r.json()["txrefs"][0]["double_spend"]
        if doubleSpend == "True":
            response = {"balance": balance, "doubleSpend": "True", "ntx": ntx}
        else:
            response = {"balance": balance, "doubleSpend": "False", "ntx": ntx}
    
    return response


# Used to check the address balance of a mainnet Bitcoin address
def checkAddressBalance(address):
    return blockexplorer.get_address(address).final_balance

# Query the BtcMarkets API to get the current bitcoin price in $AUD
def getBtcPriceInAud():
    domain = "https://api.btcmarkets.net"
    uri = "/market/BTC/AUD/tick"
    url = domain + uri
    r = requests.get(url, verify=True)
    return int(r.json()["lastPrice"])

def convertAudToBtc(amountInFiat):
    return amountInFiat / getBtcPriceInAud()